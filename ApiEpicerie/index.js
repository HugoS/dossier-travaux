const express = require('express');

const app = express();

const mysql = require('mysql2/promise');
let connection = null

// create the connection

async function instanciateDatabase() {
  return await mysql.createConnection({ host: 'localhost', user: 'root', password: 'root', database: 'dataepicerie'});
}

(async () => {
    console.log('je passe')
    connection = await instanciateDatabase()
})()

// respond with "hello world" when a GET request is made to the homepage
app.get('/', async (req, res) => {
  const [rows, fields] = await connection.execute('SELECT * FROM `dataepicerie`');
  console.log(rows)
  // console.log(rows, fields)
  res.json(rows[0].name)
});


app.listen(3000)